# README #


Implementation of Paper "Scalable Approximation Algorithm for Graph Summarization"

This Code was Developed in Eclipse Juno. 
Import the Project.
The starting page of the Implementatio is SuperGraph.java
The Algorithm operates on following paramters : 
1. GraphFile.txt in pajek format.
(We have used R to convert graphs into undirected, unweighted form, and saved them in pajek format. A sample Graph is placed
in \graphs\ directory.)
2. k (Final nodes) 
3. w (Count min sketch parameter width)
4. d (Count min sketch papameter depth)
These paramters can be modified from main() method in SuperGraph.java to test the code.

In case of concerns please email at maham.a.beg@gmail.com