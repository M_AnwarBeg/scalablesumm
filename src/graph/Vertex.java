package graph;

import java.util.HashSet;
import java.util.Set;

public class Vertex {
	private int id;
	Neighbor adjacencyListHead;
	Neighbor adjacencyListTail;
	int degree;
	double S_x;
	double e_xi;
	double e_x;
	double n_x;
	double sqrt_n_x;
	private Set<Integer> setOfVertexIds;
	double weight;
	double[][] countMinBucket;
	SamplingNode nodeRef;

	Vertex(int id) {
		countMinBucket = new double[SuperGraph.sketchDepth][SuperGraph.sketchWidth];
		this.id = id;
		degree = 0;
		this.e_x = 0;
		this.n_x = 1;
		this.sqrt_n_x = 1;
		setOfVertexIds = new HashSet<Integer>();
		setOfVertexIds.add(id);

	}

	public Set<Integer> getVertexIds() {
		return setOfVertexIds;
	}

	public void setInternalVertices(Set<Integer> vertices) {
		this.setOfVertexIds = new HashSet<Integer>(vertices);
		this.n_x = setOfVertexIds.size();
		this.sqrt_n_x = Math.sqrt(setOfVertexIds.size());

	}

	public double getMergedEdgeCount() {
		return e_x;
	}

	public void setMergedEdgeCount(double count) {
		this.e_x = count;
	}

	public double vertexCount() {
		return setOfVertexIds.size();
		// return internalVertexCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Neighbor getNeighbourHead() {

		return adjacencyListHead;
	}

	public void addAtEnd(Neighbor n) {
		if (degree == 0) {
			adjacencyListHead = n;
			adjacencyListTail = n;
			degree++;

		} else {
			adjacencyListTail.next = n;
			n.previous = adjacencyListTail;
			adjacencyListTail = n;
			degree++;
		}
		// if(n.next==null)
		// adjTail = n;

	}

	public void addNewNodeAtEnd(Neighbor n) {
		if (degree == 0) {
			adjacencyListHead = n;
			adjacencyListTail = n;
			degree++;

		} else {
			adjacencyListTail.next = n;
			n.previous = adjacencyListTail;
			adjacencyListTail = n;
			degree++;
		}
		// if(n.next==null)
		// adjTail = n;

		S_x = degree;
		e_xi = degree;
		weight = -1 * degree;

		for (int i = 0; i < SuperGraph.sketchDepth; i++) {
			// System.out.println(this.id + " neighbor: "+ n.id);
			int index = SuperGraph.sketch.hash((n.id), i);
			// System.out.println("Id :"+n.id+"  "+"index for HashFunction: " +i
			// +" Hashed at: "+ index);

			// buckets[i][index] = buckets[i][index].add(new BigDecimal(1));
			countMinBucket[i][index] = countMinBucket[i][index] + 1;
		}
	}

	public void remove(Neighbor n) {
		if (degree == 0)
			return;
		if (degree == 1) {
			// n= null;
			adjacencyListTail = null;
			adjacencyListHead = null;
		}

		else if (n.previous == null) {
			adjacencyListHead = n.next;
			adjacencyListHead.previous = null;
		}

		else if (n.next == null) {
			adjacencyListTail = n.previous;
			adjacencyListTail.next = null;
		} else {
			Neighbor previous = n.previous;
			Neighbor next = n.next;

			previous.next = n.next;
			next.previous = n.previous;
		}
		n = null;
		degree--;
	}

	public boolean isEmpty() {
		return degree == 0;
	}

	public int size() {
		return degree;
	}

}