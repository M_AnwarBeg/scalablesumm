package graph;

public class Edge implements Comparable<Edge> {
	public int start, end;

	public Edge(int start, int end) {
		this.start = start;
		this.end = end;
	}

	public boolean equals(Object o) {
		Edge e = (Edge) o;
		if (((e.start == start) && (e.end == end)))
			return true;
		if (((e.start == end) && (e.end == start)))
			return true;
		else
			return false;

	}

	public int compareTo(Edge e) {

		if (start == e.start) {
			if (end > e.end)
				return 1;
		}
		if (start == e.start) {
			if (end < e.end)
				return -1;
		}

		if (start > e.start)
			return 1;
		if (start < e.start)
			return -1;
		else
			return 0;
	}

	public String toString() {
		String temp = "[" + start + ", " + end + "]";
		return temp;
	}

}
