package graph;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Query {
	

public static double[] averageDegreeError(SuperGraph g){
	double sumAbsdegreeError = 0 ;
	double sumAbsdegreeErrorSquared =0;
	Iterator it = g.listOfVertices.entrySet().iterator();
	while (it.hasNext()) {
		double expectedDegree = 0;
		Map.Entry pair = (Map.Entry) it.next();
		Vertex superNode = (Vertex) pair.getValue();
		
		double clusterInternalDegree =0;
		if(superNode.vertexCount() > 1)
			clusterInternalDegree = 2*superNode.getMergedEdgeCount() / (superNode.vertexCount() - 1);
		else 
			clusterInternalDegree =0;
		
		double clusterOutDegree = superNode.e_xi/superNode.vertexCount();
		
		Set<Integer> internalVertices = superNode.getVertexIds();
		internalVertices.toArray();
		expectedDegree = clusterInternalDegree + clusterOutDegree;
		
		for (Integer v : internalVertices) {
			double absExpectedDegree  = Math.abs(g.vertexDegrees.get(v) - expectedDegree);			
			double squaredabsExpectedDegree = Math.pow(absExpectedDegree,2);	
			sumAbsdegreeError = sumAbsdegreeError + absExpectedDegree;
			sumAbsdegreeErrorSquared = sumAbsdegreeErrorSquared + squaredabsExpectedDegree;
		}
	}
	double avrAbsDegreeError  =  sumAbsdegreeError/g.V.doubleValue();
	double avrAbsdegreeErrorStd = Math.sqrt(sumAbsdegreeErrorSquared/g.V.doubleValue() - Math.pow(sumAbsdegreeError/g.V.doubleValue(),2));
	
	double[] degreeQuery = {avrAbsDegreeError,avrAbsdegreeErrorStd};
	return degreeQuery;
}

public static double[] centrality(SuperGraph g){

	double sumCentralityError = 0 ;
	double sumCentralityErrorSquared =0;
	
	Iterator it = g.listOfVertices.entrySet().iterator();
	while (it.hasNext()) {
		double expectedDegree = 0;
		Map.Entry pair = (Map.Entry) it.next();
		Vertex superNode = (Vertex) pair.getValue();

		double clusterInternalDegree =0;
		if(superNode.vertexCount() > 1)
			clusterInternalDegree = 2*superNode.getMergedEdgeCount() / (superNode.vertexCount() - 1);
		else 
			clusterInternalDegree =0;
		
		double clusterOutDegree = superNode.e_xi/superNode.vertexCount();
		
		Set<Integer> internalVertices = superNode.getVertexIds();
		internalVertices.toArray();
		expectedDegree = clusterInternalDegree + clusterOutDegree;
		
		for (Integer v : internalVertices) {
			double centrality_v = ((double) g.vertexDegrees.get(v))/(2*g.E);
			double expectedCentrality_v = expectedDegree/(2*g.E);
			double centralityError = Math.abs(centrality_v - expectedCentrality_v);	
			sumCentralityError = sumCentralityError + centralityError;
			sumCentralityErrorSquared = sumCentralityErrorSquared + Math.pow(centralityError,2);
			
		}
	}
	double avrCentralityError  =  sumCentralityError/g.V.doubleValue();
	double avrCentralityStd = Math.sqrt(sumCentralityErrorSquared/g.V.doubleValue() - Math.pow(sumCentralityError/g.V.doubleValue(),2));
	
	double[] centralityQuery = {avrCentralityError,avrCentralityStd};
	
	
	return centralityQuery;
}


	

	public static int numberOfTriangles(SuperGraph g) {
		int numberofTriangles = 0;
		Iterator itr = g.listOfVertices.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry pair = (Map.Entry) itr.next();
			Vertex v_i = (Vertex) pair.getValue();
			Neighbor n_i = v_i.adjacencyListHead;
			while (n_i != null) {
				Vertex v_j = g.listOfVertices.get(n_i.id);
				Neighbor n_j = v_j.adjacencyListHead;
				while (n_j != null) {
					if (v_i.getId() >= n_j.id) {
						n_j = n_j.next;
						continue;
					}
					Vertex v_k = g.listOfVertices.get(n_j.id);	
					Neighbor n_k = v_k.adjacencyListHead;
					while (n_k != null) {

						if (n_k.id == v_i.getId())
							numberofTriangles++;

						n_k = n_k.next;

					}
					n_j = n_j.next;
				}
				n_i = n_i.next;

			}
		}
		System.out.println(numberofTriangles);
		return numberofTriangles;

	}
	
public static double expectedNumberOfTriangles(SuperGraph g){
	
	double exptectedTriangles = 0 ; 
	Set<Integer> keySet = g.listOfVertices.keySet();
	Integer[] ids = keySet.toArray(new Integer[keySet
	                                     				.size()]);
	
	
	for(int i = 0 ; i < ids.length ; i ++){
	 	Vertex v_i  = g.listOfVertices.get(ids[i]);
	  	double triplets = v_i.n_x >= 3 ? v_i.n_x * (v_i.n_x - 1) * (v_i.n_x - 2) / 6.0 : 0 ;
	 	double prob_ii = (Double.isNaN((2*v_i.e_x) / (v_i.n_x*(v_i.n_x - 1)))) ? 0 
	 			: (2*v_i.e_x) / (v_i.n_x*(v_i.n_x - 1));
	 	exptectedTriangles = exptectedTriangles +  triplets * Math.pow(prob_ii, 3);
	 	Neighbor n_i = v_i.adjacencyListHead ;
	 	
 	
	 	while(n_i != null){
	 		if (n_i.id <=v_i.getId()) {
				n_i = n_i.next;
				continue;
			}
	 		Vertex v_j =  g.listOfVertices.get(n_i.id);
	 
	 		double prob_jj = (Double.isNaN((2*v_j.e_x) / (v_j.n_x*(v_j.n_x - 1))))? 0:(2*v_j.e_x) / (v_j.n_x*(v_j.n_x - 1));
	 	 	double pairs_i = v_i.n_x >= 2 ? (v_i.n_x *(v_i.n_x -1))/2: 0;
	 	 	double pairs_j = v_j.n_x >= 2 ? (v_j.n_x *(v_j.n_x -1))/2: 0;

	 	 	double prob_ij  = n_i.edgeWeight/(v_i.n_x*v_j.n_x);
	 	 	double first_case =  pairs_i * v_j.n_x *prob_ii  ;  
	 	 	double second_case =  pairs_j * v_i.n_x* prob_jj  ; 
	 	 	exptectedTriangles = exptectedTriangles+Math.pow(prob_ij, 2) * (first_case + second_case);	 	 	
	 	
	 		
		 	Neighbor n_j = v_j.adjacencyListHead ;
	
	 	while(n_j != null){
			if (n_j.id <=v_j.getId()) {
				n_j = n_j.next;
				continue;
			}
			 	Vertex v_w =  g.listOfVertices.get(n_j.id);
	 	 	 	Neighbor n_w = v_w.adjacencyListHead;
		 		double weight_jw = n_j.edgeWeight;
	 	 	 	while(n_w != null){
	 	 	 	
	 	 			if(n_w.id ==v_i.getId()){	 	 				
	 	 		 	 	double prob_iw  = n_w.edgeWeight/(v_i.n_x*v_w.n_x);
	 	 		 	 	double prob_jw  = weight_jw/(v_j.n_x*v_w.n_x);
	 	 		 	 	
	 	 		 	 	exptectedTriangles = exptectedTriangles +( v_i.n_x*v_j.n_x*v_w.n_x
	 	 		 	 			*prob_ij*prob_iw*prob_jw);
	 	 		 	// n_j = n_j.next;
	 	 		 	break;
	 	 		 	 	
	 	 			}
	 	 			n_w = n_w.next;
	 	 		}
	 		 	 n_j = n_j.next;

	 		}
	 		n_i = n_i.next;
	 	
	 	}

		
		
	}
//	System.out.println(exptectedTriangles);
	return exptectedTriangles;
	
}


//(expected-exact)/exact
public static double relativeTraiangleDensity(SuperGraph g){
	double relativeDensity = 0;
	double exactnumberofTriangles = g.numberofTriangles;
	double expectedNumberofTriangles = expectedNumberOfTriangles(g);
	relativeDensity = (expectedNumberofTriangles - exactnumberofTriangles) / exactnumberofTriangles;
	return expectedNumberofTriangles;
}


public static double expectedClusteringCoeffecient(SuperGraph g){
	    return  (6 * expectedNumberOfTriangles(g)) / (g.V.intValue() * (g.V.intValue()-1) * (g.V.intValue()-2));
	}	
	


}

