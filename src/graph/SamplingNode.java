package graph;

public class SamplingNode {
	int id;
	double weight;

	SamplingNode leftChild;
	SamplingNode rightChild;
	SamplingNode parent;

	SamplingNode() {
		id = -1;
		weight = -1;
		leftChild = rightChild = parent = null;
	}

	SamplingNode(int nodeId, double nodeWeight) {
		this.id = nodeId;
		this.weight = nodeWeight;
		this.leftChild = null;
		this.rightChild = null;
		this.parent = null;
	}

}
