package graph;

public class Neighbor {
	int id;
	Neighbor next;
	Neighbor previous;
	double edgeWeight;
	Neighbor corresponding;

	public int id() {
		return id;
	}

	public double weight() {
		return edgeWeight;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setWeight(double weight) {
		this.edgeWeight = weight;
	}

	public void setCorresponding(Neighbor n) {

		this.corresponding = n;

	}

	Neighbor() {
		/*
		 * this.id = ; this.weight = weight; this.corresponding = corresponding;
		 */
	}

	Neighbor(int id, double weight, Neighbor corresponding) {
		this.id = id;
		this.edgeWeight = weight;
		this.corresponding = corresponding;
	}

	/*
	 * public boolean hasNext(){ if(this.next!=null) return true; else return
	 * false;
	 * 
	 * }
	 */

	public void remove() {

		if (this.previous == null) {

		}

		if (this.next != null) {
			this.next.previous = this.previous;
			this.previous = this.next;
		} else
			this.previous.next = null;

	}

	public void remove(Neighbor n) {

		if (n.next != null) {
			n.next.previous = n.previous;
			n.previous = n.next;
		} else
			n.previous.next = null;

	}

	public void addBefore(Neighbor item) {
		if (this.previous == null) {
			this.previous = item;
			item.next = this;
		} else {
			this.previous.next = item;
			this.previous = item;
			item.next = this;
		}
	}

	public void addAfter(Neighbor item) {
		if (this.next == null) {
			this.next = item;
			item.previous = this;
		} else {
			item.next = this.next;
			item.previous = this;
			this.next = item;
		}

	}
}
