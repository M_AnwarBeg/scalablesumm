package graph;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


public class SuperGraph{
	public static final double NaN = 0.0d / 0.0;
	int E;
	BigDecimal V;
	public HashMap<Integer, Vertex> listOfVertices;
	int latestVertexId;
	int numberOfSuperNodes;
	Vertex newVertex;
	HashMap<Integer, Integer> vertexDegrees;
	static CountMinSketch sketch;
	static int sketchWidth;
	static int sketchDepth;
	Integer vertexids[];
	ArrayList<Integer> vertexIds;

	SamplingNode SamplingTreeRoot;
	SamplingNode SamplingTreeFirstEmptyNode;
	double numberofTriangles;

	String graphFileName;
	ArrayList<String> summaryError;
	static long startTime;

	int jumps;
	SuperGraph() {
		listOfVertices = new HashMap<Integer, Vertex>();
		summaryError = new ArrayList<>();
	}

	public void vertexIds() {
		vertexIds = new ArrayList<Integer>();
		for (Map.Entry<Integer, Vertex> entry : listOfVertices.entrySet()) {
			vertexIds.add(entry.getKey());
			Vertex v = entry.getValue();
		}
	}

	public SamplingNode buildSamplingTree(int st, int end) {
		if (end - st == 0) {

			double treeWeight = listOfVertices.get(vertexids[st]).weight;
			if (treeWeight == 0) {
				System.out.println("Weight is zero");
				treeWeight = -1 / Double.MIN_VALUE;
				System.out.println("Value in tree = " + treeWeight);
			}

			else
				treeWeight = -1 / treeWeight;
			SamplingNode crntNode = new SamplingNode(listOfVertices.get(
					vertexids[st]).getId(), treeWeight);
			listOfVertices.get(vertexids[st]).nodeRef = crntNode;
			return crntNode;
		} else {
			int mid = (end + st) / 2;
			SamplingNode left = buildSamplingTree(st, mid);
			SamplingNode right = buildSamplingTree(mid + 1, end);

			SamplingNode crntNode = new SamplingNode(-1, left.weight
					+ right.weight);
			crntNode.leftChild = left;
			crntNode.rightChild = right;
			left.parent = crntNode;
			right.parent = crntNode;
			return crntNode;
		}
	}

	public void updateSamplingTree(SamplingNode crntNode) {
		try {
			double x, y;

			while (crntNode != null) {
				x = 0.0;
				y = 0.0;
				if (crntNode.leftChild.weight != -1)
					x = crntNode.leftChild.weight;
				if (crntNode.rightChild.weight != -1)
					y = crntNode.rightChild.weight;
				crntNode.weight = x + y;

				crntNode = crntNode.parent;
			}
		} catch (Exception exp) {

			System.out.println("updateSamplingTree exception");
		}

	}

	public void deleteSamplingNode(SamplingNode crntNode) {

		crntNode.weight = 0.0;
		updateSamplingTree(crntNode.parent);
		crntNode.id = -1;
		crntNode.weight = -1;

	}

	public ArrayList<Pair> samplePairsWeighted_Treebased(int size) {

		ArrayList<Pair> samplePairs = new ArrayList<Pair>();
		while (samplePairs.size() < size) {

			Random random = new Random();
			double value = random.nextDouble() * SamplingTreeRoot.weight;
			int v1 = getLeaf(value, SamplingTreeRoot);
			value = random.nextDouble() * SamplingTreeRoot.weight;
			int v2 = getLeaf(value, SamplingTreeRoot);
			if (v1 != v2) {
				Pair p = new Pair(v1, v2);
				samplePairs.add(p);
				// System.out.println(v1+","+v2);
			}
		}
		return samplePairs;
	}

	public int getLeaf(double value, SamplingNode crnt) {

		if (crnt.leftChild == null && crnt.rightChild == null && crnt.id != -1)
			return crnt.id;
		else {
			if (value < crnt.leftChild.weight)
				return getLeaf(value, crnt.leftChild);
			else
				return getLeaf(value - crnt.leftChild.weight, crnt.rightChild);
		}
	}

	public void vertexDegrees() {

		int maxDegree = 0;
		int minDegree = Integer.MAX_VALUE;
		vertexDegrees = new HashMap<Integer, Integer>();
		Iterator itr = listOfVertices.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry pair = (Map.Entry) itr.next();
			Vertex v = (Vertex) pair.getValue();
			if (v.degree > maxDegree)
				maxDegree = v.degree;
			if (v.degree < minDegree) {
				minDegree = v.degree;
			}
			vertexDegrees.put(v.getId(), v.degree);
			E = E + v.degree;

		}
		System.out.println("Min Degree: " + minDegree);
		System.out.println("Max Degree: " + maxDegree);
	}

	// Reads Graph in Pajek Format
	public ArrayList<Edge> readGraph(String filename) {
		sketch = new CountMinSketch(sketchWidth, sketchDepth);
		ArrayList<Edge> edges = new ArrayList<Edge>();
		try {
			FileReader filein = new FileReader(filename);
			BufferedReader graphFile = new BufferedReader(filein);
			String inLine;
			String[] parts;
			parts = filename.substring(0, filename.length() - 4).split("_");
			inLine = graphFile.readLine();
			if (inLine != null) {
				parts = inLine.split("\\s");
				// System.out.println("|V|: "+ parts[1]);
				latestVertexId = Integer.parseInt(parts[1]);
				// numberOfSuperNodes = Integer.parseInt(parts[1]);
			}
			while ((inLine = graphFile.readLine()) != null) {
				parts = inLine.split("\\s");
				try {
					if (!checkFormat(parts[0]) || parts.length < 2) {
						System.err.println("Line Format is wrong: Skipping "
								+ inLine);
						continue;
					}
					Edge e = new Edge(Integer.parseInt(parts[0]),
							Integer.parseInt(parts[1]));
					edges.add(e);

				} catch (NumberFormatException e) {
					System.err.println("Line Format is wrong: Skipping "
							+ inLine);
				}
			}

		} catch (IOException e) {
			System.err.println(e);
		}
		Collections.sort(edges);
		return edges;
	}

	void populateGraph(String path) {

		ArrayList<Edge> edges = readGraph(path);
		Set vertices = new HashSet<Integer>();

		for (Edge e : edges) {
			Vertex v1;
			Vertex v2;
			if (!vertices.contains(e.start)) {
				vertices.add(e.start);
				v1 = new Vertex(e.start);
				Vertex v1Detail = new Vertex(e.start);
				listOfVertices.put(e.start, v1);
			}
			if (!vertices.contains(e.end)) {
				vertices.add(e.end);
				v2 = new Vertex(e.end);
				Vertex v2Detail = new Vertex(e.end);
				listOfVertices.put(e.end, v2);
			}
			v1 = listOfVertices.get(new Integer(e.start));
			v2 = listOfVertices.get(new Integer(e.end));
			Neighbor n = new Neighbor();
			n.setID(e.end);
			n.setWeight(1);
			v1.addNewNodeAtEnd(n);
			Neighbor n2 = new Neighbor();
			n2.setID(e.start);
			n2.setWeight(1);
			n.setCorresponding(n2);
			n2.setCorresponding(n);
			v2.addNewNodeAtEnd(n2);
		}
		numberOfSuperNodes = listOfVertices.size();
		V = new BigDecimal(listOfVertices.size());
		System.out.println("|V|: " + V);
		System.out.println("|E|: " + edges.size());

		System.out.println("List Size: " + listOfVertices.size());
	}

	void merge(Pair pair) {
		// long t_1 = System.currentTimeMillis();
		int v1 = pair.getNode1();
		int v2 = pair.getNode2();
		latestVertexId++;
		Vertex vertex1 = listOfVertices.get(v1);
		Vertex vertex2 = listOfVertices.get(v2);
		Set<Integer> mergedVertices = new TreeSet<Integer>(
				vertex1.getVertexIds());
		mergedVertices.addAll(vertex2.getVertexIds());

		newVertex = new Vertex(latestVertexId);
		newVertex.setInternalVertices(mergedVertices);

		double collapsedEdgeWeight = 0;
		Neighbor adjListIterator1 = vertex1.adjacencyListHead;
		Neighbor adjListIterator2 = vertex2.adjacencyListHead;

		if (adjListIterator1 == null && adjListIterator2 != null) {
			adjListIterator1 = adjListIterator2;
			adjListIterator2 = null;
		}

		while (adjListIterator1 != null) {
			if (adjListIterator2 == null) {
				if (adjListIterator1.id() == v1 || adjListIterator1.id() == v2)
					collapsedEdgeWeight = adjListIterator1.edgeWeight;
				else {
					removeOldInformation(adjListIterator1);
					updateAdjacencyList(adjListIterator1);
					updateNewInformation(adjListIterator1);

				}
				adjListIterator1 = adjListIterator1.next;
			} else if (adjListIterator1.id() == adjListIterator2.id()) {
				removeOldInformation(adjListIterator2);
				removeOldInformation(adjListIterator1);
				adjListIterator1.setWeight(adjListIterator1.weight()
						+ adjListIterator2.weight());

				updateAdjacencyList(adjListIterator1);
				updateNewInformation(adjListIterator1);

				Vertex v = listOfVertices.get(adjListIterator2.id());
				v.remove(adjListIterator2.corresponding);
				adjListIterator1 = adjListIterator1.next;
				adjListIterator2 = adjListIterator2.next;
			} else if (adjListIterator1.id() < adjListIterator2.id()) {

				if (adjListIterator1.id() == v1 || adjListIterator1.id() == v2) {
					collapsedEdgeWeight = adjListIterator1.weight();

				} else {
					removeOldInformation(adjListIterator1);
					updateAdjacencyList(adjListIterator1);
					updateNewInformation(adjListIterator1);

				}
				adjListIterator1 = adjListIterator1.next;
			} else if (adjListIterator1.id() > adjListIterator2.id()) {
				if ((adjListIterator2.id() == v1 || adjListIterator2.id() == v2)) {
					collapsedEdgeWeight = adjListIterator2.weight();
				} else {
					removeOldInformation(adjListIterator2);
					updateAdjacencyList(adjListIterator2);
					updateNewInformation(adjListIterator2);

				}
				adjListIterator2 = adjListIterator2.next;
			}

			if (adjListIterator1 == null && adjListIterator2 != null) {
				adjListIterator1 = adjListIterator2;
				adjListIterator2 = null;
			}

		}
		newVertex.setMergedEdgeCount(collapsedEdgeWeight
				+ vertex1.getMergedEdgeCount() + vertex2.getMergedEdgeCount());

		double e_x = newVertex.getMergedEdgeCount();
		double n_x = newVertex.vertexCount();
		double e_x2 = e_x * e_x;
		double f1 = (Double.isNaN(e_x2 / (n_x * (n_x - 1)))) ? 0 : e_x2
				/ (n_x * (n_x - 1));
		double f2 = newVertex.S_x / n_x;

		newVertex.weight = -1 * (f1 + f2);
		deleteSamplingNode(vertex1.nodeRef);
		SamplingTreeFirstEmptyNode = vertex1.nodeRef;
		deleteSamplingNode(vertex2.nodeRef);

		listOfVertices.remove(v1);
		listOfVertices.remove(v2);
		listOfVertices.put(latestVertexId, newVertex);

		SamplingTreeFirstEmptyNode.id = latestVertexId;
		if (newVertex.weight == 0) {
			newVertex.weight = -1 / Double.MIN_VALUE;
		}

		else {
			SamplingTreeFirstEmptyNode.weight = -1 / newVertex.weight;
		}
		listOfVertices.get(latestVertexId).nodeRef = SamplingTreeFirstEmptyNode;
		updateSamplingTree(SamplingTreeFirstEmptyNode.parent);

		numberOfSuperNodes--;
		/*
		 * long t_2 = System.currentTimeMillis(); if(numberOfSuperNodes % jumps
		 * ==0 ){ long time = t_2-t_1; System.out.println("Merge Computation: "
		 * + time);
		 * 
		 * }
		 */

	}

	public void updateAdjacencyList(Neighbor n) {
		Vertex v = listOfVertices.get(n.id());
		v.remove(n.corresponding);
		Neighbor n1 = new Neighbor(n.id(), n.weight(), null);// for merged
																// list
		Neighbor n2 = new Neighbor(latestVertexId, n.weight(), null); // to add
																		// the
		// older
		// list
		n1.corresponding = n2;
		n2.corresponding = n1;
		v.addAtEnd(n2);
		newVertex.addAtEnd(n1);
	}

	public void removeOldInformation(Neighbor n) {
		Vertex neighbour = listOfVertices.get(n.id);
		Vertex vd = listOfVertices.get(n.corresponding.id);
		neighbour.S_x = neighbour.S_x
				- ((n.edgeWeight * n.edgeWeight) / vd.n_x);
		neighbour.e_xi = neighbour.e_xi - n.edgeWeight;

		double weight = round((n.edgeWeight) / (vd.sqrt_n_x));
		for (int i = 0; i < sketchDepth; i++) {

			int index = sketch.hash(vd.getId(), i);
			if (neighbour.countMinBucket[i][index] < 0) {
				System.out.println("Negative"); // Could occur in case of double
												// overflow
			}
			neighbour.countMinBucket[i][index] = round(neighbour.countMinBucket[i][index]
					- weight);
			if (neighbour.countMinBucket[i][index] < 0) {
				System.out.println("Negative");
			}
		}
	}

	public void updateNewInformation(Neighbor n) {
		Vertex neighbour = listOfVertices.get(n.id);
		neighbour.S_x = neighbour.S_x
				+ ((n.edgeWeight * n.edgeWeight) / newVertex.n_x);
		neighbour.e_xi = neighbour.e_xi + n.edgeWeight;

		newVertex.S_x = newVertex.S_x
				+ ((n.edgeWeight * n.edgeWeight) / neighbour.n_x);
		newVertex.e_xi = newVertex.e_xi + n.edgeWeight;

		double neighbourInformation = round((n.edgeWeight / neighbour.sqrt_n_x));
		double newVertexInformation = round((n.edgeWeight / newVertex.sqrt_n_x));
		for (int i = 0; i < sketchDepth; i++) {
			int neighbourIndex = sketch.hash(neighbour.getId(), i);
			int newVertexIndex = sketch.hash(newVertex.getId(), i);
			newVertex.countMinBucket[i][neighbourIndex] = round(newVertex.countMinBucket[i][neighbourIndex]
					+ neighbourInformation);
			neighbour.countMinBucket[i][newVertexIndex] = round(neighbour.countMinBucket[i][newVertexIndex]
					+ newVertexInformation);

		}

		double e_x = neighbour.getMergedEdgeCount();
		double n_x = neighbour.vertexCount();
		double e_x2 = e_x * e_x;
		double f1 = (Double.isNaN(e_x2 / (n_x * (n_x - 1)))) ? 0 : e_x2
				/ (n_x * (n_x - 1));
		double f2 = neighbour.S_x / n_x;
		neighbour.weight = -1 * (f1 + f2);

		if (neighbour.weight == 0) {
			System.out.println("Weight is zero");
			neighbour.nodeRef.weight = -1 / Double.MIN_VALUE;
			System.out.println("Value in tree = " + neighbour.nodeRef.weight);
		}

		else {
			neighbour.nodeRef.weight = -1 / neighbour.weight;
		}
		updateSamplingTree(neighbour.nodeRef.parent);
	}

	public HashSet<Integer> getRandomNode(int sampleSize) {
		Random generator = new Random();
		Object[] values = listOfVertices.keySet().toArray();
		HashSet<Integer> vertices = new HashSet<Integer>();
		while (vertices.size() < sampleSize) {
			int v1 = Integer.parseInt(values[generator.nextInt(values.length)]
					.toString());
			vertices.add(v1);
		}
		return vertices;

	}

	public double approxDotProduct(Pair pair) {
		int a = pair.getNode1();
		int b = pair.getNode2();
		Vertex v_a = listOfVertices.get(a);
		Vertex v_b = listOfVertices.get(b);
		double minDotProduct = Integer.MAX_VALUE;
		for (int i = 0; i < sketchDepth; i++) {
			double dotProduct = 0;
			for (int j = 0; j < sketchWidth; j++) {
				dotProduct = dotProduct + v_a.countMinBucket[i][j]
						* v_b.countMinBucket[i][j];
			}
			if (dotProduct < minDotProduct)
				minDotProduct = dotProduct;
		}
		return minDotProduct;
	}

	public ArrayList<String> Summerize(int k, boolean exact,
			boolean computeQuery) {

		numberOfSuperNodes = listOfVertices.size();
		System.out.println("Size of List " + numberOfSuperNodes);
		System.out.println("Nomalize" + V);
		String resultFileHeader = "n(t),RE,L1Error,L2Error ,averageDegreeError,stdDegreeError ,averageCentralityError,stdCentralityError ,expectedNumberofTriangles";
			summaryError.add(resultFileHeader);

		String error = null;
		while (k <= numberOfSuperNodes) {
			int sampleSize = (int) Math.ceil(log(numberOfSuperNodes, 2));
			 sampleSize = numberOfSuperNodes;
			// sampleSize = 5*sampleSize;
			 //sampleSize = sampleSize * sampleSize;
			ArrayList<Pair> nodes = samplePairsWeighted_Treebased(sampleSize);
			Iterator sampleItr = nodes.iterator();
			double maxScore = -1 * Double.MAX_VALUE;
			Pair bestScoringPair = null;
			while (sampleItr.hasNext()) {
				Pair p = (Pair) sampleItr.next();
				double score = computeScore(p, exact);
				Pair pair = new Pair(p.getNode1(), p.getNode2(), score);
				if (score > maxScore) {
					bestScoringPair = pair;
					maxScore = score;
				}
			}			
			if (bestScoringPair != null)
				merge(bestScoringPair);
			if (numberOfSuperNodes % jumps == 0) {
				
				BigDecimal denominator = new BigDecimal(reconstructionError());
				BigDecimal numerator = V;
				BigDecimal reconstructionError = denominator.divide(numerator,
						MathContext.DECIMAL64);
				System.out
						.println("Number of Supernodes " + numberOfSuperNodes);
				System.out.println("RE Error: " + reconstructionError);
				BigDecimal L1Error = denominator.divide(numerator,
						MathContext.DECIMAL64);
				System.out.println("L1 Error: " + L1Error);
				denominator = new BigDecimal(L2error());
				BigDecimal L2Error = denominator.divide(numerator,
						MathContext.DECIMAL64);
				System.out.println("L2 Error: " + L2error() / (V.intValue()));
				error = numberOfSuperNodes +"," + reconstructionError + "," + L1Error + ","+ L2Error;
				if (computeQuery) {
					double[] averageDegreeError = Query
							.averageDegreeError(this);
					double[] averageCentralityError = Query.centrality(this);
					double expectedNumberofTriangles = Query
							.expectedNumberOfTriangles(this);
					error = error + "," + Double.toString(averageDegreeError[0])
							+ "," + averageDegreeError[1] + ","
							+ averageCentralityError[0] + ","
							+ averageCentralityError[1] + ","
							+ +expectedNumberofTriangles;
				}

				summaryError.add(error);
			}
		}
		System.out.println("Done");
		return summaryError;
	}

	
	public int getCurrentVertexId() {
		return latestVertexId;
	}

	boolean checkFormat(String number) {
		boolean isInt = false;
		try {
			Integer.parseInt(number);
			isInt = true;
		} catch (NumberFormatException e) {

		}
		return isInt;
	}

	public double computeScore(Pair p, boolean exact) {

		long t_1 = System.currentTimeMillis();
		Vertex vertexa = listOfVertices.get(p.getNode1());
		Vertex vertexb = listOfVertices.get(p.getNode2());
		double e_a = vertexa.getMergedEdgeCount();
		double e_b = vertexb.getMergedEdgeCount();

		double n_a = vertexa.vertexCount();
		double n_b = vertexb.vertexCount();
		double e_ab = 0;
		if (exact) {
			e_ab = e_ab(p);
		} else
			e_ab = (vertexa.e_xi + vertexb.e_xi)
					/ (2 * (numberOfSuperNodes - 1));
		double s_a = vertexa.S_x;
		double s_b = vertexb.S_x;
		double e_a2 = e_a * e_a;
		double e_b2 = e_b * e_b;
		double S_ab = 0;
		if (exact)
			S_ab = exactDotProdutct(p);
		else
			S_ab = approxDotProduct(p);
		double temp1 = (Double.isNaN(8 * e_a2 / (n_a * (n_a - 1)))) ? 0 : 8
				* e_a2 / (n_a * (n_a - 1));
		double temp2 = (Double.isNaN(8 * e_b2 / (n_b * (n_b - 1)))) ? 0 : 8
				* e_b2 / (n_b * (n_b - 1));
		double score = -temp1
				- temp2
				+ (4 * e_ab * e_ab)
				/ (n_a * n_b)
				- (4 / n_a)
				* s_a
				- (4 / n_b)
				* s_b
				+ 8
				* ((e_a + e_b + e_ab) * (e_a + e_b + e_ab))
				/ ((n_a + n_b) * (n_a + n_b - 1))
				+ (4 / (n_a + n_b))
				* (s_a - ((e_ab * e_ab) / n_a) + s_b - ((e_ab * e_ab) / n_b) + 2 * S_ab);
		long t_2 = System.currentTimeMillis();
		if (numberOfSuperNodes % jumps == 0) {
			long time = t_2 - t_1;
			// System.out.println("Score Computation: "+ exact +" " + time);

		}
		return score;
	}

	public double e_ab(Pair p) {
		int a = p.getNode1();
		int b = p.getNode2();
		double e_ab = 0;
		Neighbor itr = listOfVertices.get(a).adjacencyListHead;
		while (itr != null) {
			if (itr.id == b) {
				e_ab = itr.edgeWeight;
				itr = itr.next;
				continue;
			}
			itr = itr.next;
		}
		return e_ab;

	}

	public double exactDotProdutct(Pair p) {
		double e_ae_b = 0;
		Vertex vertex1 = listOfVertices.get(p.getNode1());
		Vertex vertex2 = listOfVertices.get(p.getNode2());

		Neighbor v1Itr = vertex1.adjacencyListHead;
		Neighbor v2Itr = vertex2.adjacencyListHead;

		if (v1Itr == null && v2Itr != null) {
			v1Itr = v2Itr;
			v2Itr = null;
		}

		while (v1Itr != null) {
			if (v2Itr == null) {
				v1Itr = v1Itr.next;
			}

			else if (v1Itr.id() == v2Itr.id()) {
				e_ae_b = e_ae_b + (v1Itr.weight() * v2Itr.weight())
						/ (listOfVertices.get(v2Itr.id()).n_x);
				v1Itr = v1Itr.next;
				v2Itr = v2Itr.next;
			} else if (v1Itr.id() != v2Itr.id()) {

				if (v1Itr.id() < v2Itr.id()) {
					v1Itr = v1Itr.next;
				} else if (v1Itr.id() > v2Itr.id()) {
					v2Itr = v2Itr.next;
				}
			}
			if (v1Itr == null && v2Itr != null) {
				v1Itr = v2Itr;
				v2Itr = null;
			}
		}
		return e_ae_b;

	}

	public ArrayList<Pair> getSamplePairs(double c) {
		int numberOfPairs = (int) (c * numberOfSuperNodes);
		Random generator = new Random();
		Object[] values = listOfVertices.keySet().toArray();
		ArrayList<Pair> samplePairs = new ArrayList<Pair>();
		while (samplePairs.size() < numberOfPairs) {
			int v1 = Integer.parseInt(values[generator.nextInt(values.length)]
					.toString());
			int v2 = Integer.parseInt(values[generator.nextInt(values.length)]
					.toString());
			if (v1 != v2) {
				Pair p = new Pair(v1, v2);
				samplePairs.add(p);
			}
		}
		return samplePairs;
	}

	public ArrayList<Pair> getSamplePairs(int numberOfPairs) {

		Random generator = new Random();
		Object[] values = listOfVertices.keySet().toArray();
		ArrayList<Pair> samplePairs = new ArrayList<Pair>();
		while (samplePairs.size() < numberOfPairs) {
			int v1 = Integer.parseInt(values[generator.nextInt(values.length)]
					.toString());
			int v2 = Integer.parseInt(values[generator.nextInt(values.length)]
					.toString());
			if (v1 != v2) {
				Pair p = new Pair(v1, v2);
				samplePairs.add(p);
			}
		}
		if (samplePairs.size() % 100 == 0) {
			System.out.println("random Sample pairs " + samplePairs.size());
		}
		return samplePairs;
	}

	// Calculate RE by 4*e_i - 4*e_i^2/n_i C 2 + 4e_ij - 4e_ij^2/n_i.n_j of
	// entire Graph in O(n(t)) time
	public double reconstructionError() {
		double formulaRE = 0.0;
		for (Map.Entry<Integer, Vertex> entry : listOfVertices.entrySet()) {
			Vertex vd = entry.getValue();
			double term = (Double.isNaN(8 * (vd.e_x * vd.e_x)
					/ (vd.n_x * (vd.n_x - 1)))) ? 0 : 8 * (vd.e_x * vd.e_x)
					/ (vd.n_x * (vd.n_x - 1));
			formulaRE = formulaRE + (4 * vd.e_x - term) + 2
					* (vd.e_xi - (vd.S_x / vd.n_x));
		}
		return formulaRE;
	}

	// L2 error Defined by "Graph Summerization With Quality Gaurentees"
	public double L2error() {
		double formulaRE = 0.0;
		for (Map.Entry<Integer, Vertex> entry : listOfVertices.entrySet()) {
			Vertex vd = entry.getValue();
			// double e_x = 2*vd.e_x;
			double term = 4 * (vd.e_x * vd.e_x) / (vd.n_x * (vd.n_x));
			if (Double.isNaN(term)) {
				term = 0;
			}
			formulaRE = formulaRE + (2 * vd.e_x - term)
					+ (vd.e_xi - (vd.S_x / vd.n_x));
		}
		return formulaRE;
	}

	// L1 error Defined by "Graph Summerization With Quality Gaurentees"
	public double L1error() {
		double formulaRE = 0.0;
		for (Map.Entry<Integer, Vertex> entry : listOfVertices.entrySet()) {
			Vertex vd = entry.getValue();
			double term = 8 * (vd.e_x * vd.e_x) / (vd.n_x * (vd.n_x));
			if (Double.isNaN(term)) {
				term = 0;
			}
			formulaRE = formulaRE + (4 * vd.e_x - term) + 2
					* (vd.e_xi - (vd.S_x / vd.n_x));
		}
		return formulaRE;

	}


	public static void summerizeWithSketch(String path, int k, int width,
			int depth, int jump, boolean query) {
		sketchWidth = width;
		sketchDepth = depth;
		SuperGraph g = new SuperGraph();
		g.graphFileName = path;
		g.populateGraph(path);
		System.out.println("Graph Populated!");
		g.vertexIds();
		g.vertexids = g.vertexIds.toArray(new Integer[g.vertexIds.size()]);
		g.jumps = jump;
		boolean computeQuery = query;
		System.out.println("Approximate Score");
		if (computeQuery) {
			g.vertexDegrees();
			
			// Exact triangle count can be taken from snap.stanford.edu/data/
			//Computing triangles for Orignal graph is O(n^3) 
			
			// g.numberofTriangles = 1612010; 
			// =Query.numberOfTriangles(g);
		}
		startTime = System.currentTimeMillis();
		g.SamplingTreeRoot = g.buildSamplingTree(0, g.vertexids.length - 1);
		g.Summerize(k, false, computeQuery);
		long endTime = System.currentTimeMillis();
		String parameters = null;
		parameters = "_k-" + k + "_SketchWidth-" + sketchWidth
				+ "_SketchDepth-" + sketchDepth;
		long milliseconds = endTime - startTime;
		System.out.println("sketch Witdth  " + sketchWidth);

		System.out.println("k           " + k);
		System.out.println("Runtime ms  " + milliseconds);

		System.out.println("Runtime     " + milliseconds / 1000.0f + " (s)");

		writeResultstoFile(g.summaryError, path, parameters,
				milliseconds);

	}

	public static void summerizeWithExactScore(String path, int k, int jump,boolean query) {

		SuperGraph g = new SuperGraph();
		g.graphFileName = path;
		g.populateGraph(path);
		System.out.println("Graph Populated!");
		g.vertexIds();
		g.vertexids = g.vertexIds.toArray(new Integer[g.vertexIds.size()]);
		g.jumps = jump;
		boolean computeQuery = query;
		System.out.println("Exact Score");

		if (computeQuery) {
			g.vertexDegrees();
			// g.numberofTriangles = 1612010;
			// g.numberofTriangles
			// =Query.numberOfTriangles(g);
		}

		startTime = System.currentTimeMillis();
		g.SamplingTreeRoot = g.buildSamplingTree(0, g.vertexids.length - 1);
		g.Summerize(k, true, computeQuery);
		long endTime = System.currentTimeMillis();
		long milliseconds = endTime - startTime;
		System.out.println("k           " + k);
		System.out.println("Runtime     " + milliseconds / 1000.0 + " (s)");
		System.out.println("Exact Score " + true);
		String parameters = null;
		parameters = "_k-" + k;
		writeResultstoFile(g.summaryError, path, parameters,
				milliseconds);

	}

	public static void main(String[] args) throws IOException{
		String path = "graphs\\Facebook.txt";
		int k = 500;
		int jump = 100;  // record Errors after every "jump" iterations
		summerizeWithExactScore(path, k, jump,true);
		int w = 50;
		int d = 2;
		summerizeWithSketch(path, k, w, d, jump, true);

		
		System.out.println("Done");
	}

	public int log(int x, int base) {
		return (int) (Math.log(x) / Math.log(base));
	}

	public double round(double number) {
		return Math.round(number * 1000000.0) / 1000000.0;
	}
	
	public static void writeResultstoFile(ArrayList<String> data,String fileName,String detail,long runTime){

		if (fileName.endsWith(".txt")) {
			fileName = fileName.substring(0, fileName.length() - 4);
			fileName = fileName+"_"+detail;
			fileName = fileName + ".csv";
		}
		float seconds = runTime / 1000.0f;
		float minutes = runTime / (1000.0f * 60f);
	    float hours = runTime / (1000.0f * 60f * 60f);
	  	String runtime = "Seconds: "+ seconds+ " Minutes: " +minutes + " Hours: " + hours;
	    File file = new File(fileName);
		Iterator it = data.iterator();
		try {
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
	        BufferedWriter bw = new BufferedWriter(fw);
	        while (it.hasNext()) {
	        	String value = (String) it.next();
				bw.write(value);
	            bw.append(System.getProperty("line.separator"));
			}	        
	        bw.append(runtime);
	        bw.close();
		} catch (Exception exp) {
			System.out.println(exp.getMessage());
		}
	}

}
